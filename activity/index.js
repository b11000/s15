/*2. Create an index.js file and console log the message Hello World to ensure that the script file is properly associated with the html file.*/
console.log("Hello World!")

/*3. Prompt the user for 2 numbers and perform different arithmetic operations based on the total of the two numbers:
- If the total of the two numbers is less than 10, add the numbers
- If the total of the two numbers is 10 - 20, subtract the numbers
- If the total of the two numbers is 21 - 30 multiply the numbers
- If the total of the two numbers is greater than or equal to 31, divide the numbers*/

let num1 = parseInt(prompt("provide a number:"));
let num2 = parseInt(prompt("provide another number:"));

	if(num1 + num2 <= 9){
		alert("The sum of two numbers are: " + (num1 + num2));
	}
	else if(num1 + num2 >= 10 && num1 + num2 <= 20){
		alert("The difference of the two numbers are: " + (num1 - num2));
	}
	else if(num1 + num2 >= 21 && num1 + num2 <= 30){
		alert("The Product of the two numbers are: " + (num1 * num2));
	}
	else{
		alert("The Quotient of the two numbers are: " + (num1 / num2));
	}


// 4. Use an alert for the total of 10 or greater and a console warning for the total of 9 or less.

let totalNum = num1 + num2;
	if(totalNum >= 10){
		alert("Alert! Total of 10 or greater");
	}
	else{
		console.warn("Warning! Total of 9 or less");
	}

/*5. Prompt the user for their name and age and print out different alert messages based on the user input:
- If the name OR age is blank/null, print the message are you a time traveler?
- If the name AND age is not blank, print the message with the user’s name and age.*/


let name = prompt("What is your name? ");
let age = parseInt(prompt("What is your age? "));
	if(name == '' || age == ''){
		alert("Are you a time traveler? ");
	}
	else if(name != '' && age != ''){
		alert("Hello " + name + ". " + "Your age is " + age + " years old.");
	}

/*6. Create a function named isLegalAge which will check if the user's input from the previous prompt is of legal age:
- 18 or greater, print an alert message saying You are of legal age.
- 17 or less, print an alert message saying You are not allowed here.*/

function isLegalAge(age){
	if(age >= 18){
		alert("You are of legal age. ");
	}
	else{
		alert("You are not allowed here. ")
	}
}
message = isLegalAge(age);
console.log(message);

// 8. Create a switch case statement that will check if the user's age input is within a certain set of expected input:
// - 18 - print the message You are now allowed to party.
// - 21 - print the message You are now part of the adult society.
// - 65 - print the message We thank you for your contribution to society.
// - Any other value - print the message Are you sure you're not an alien?

switch (age){
			case 18:
				alert("You are now allowed to party.");
				break;
			case 21:
				alert("You are now part of the adult society.")
				break;
			case 65:
				alert("We thank you for your contribution to society")
				break;
			default:
				alert("Are you sure you're not an alien?");
		}

// 9. Create a try catch finally statement to force an error, print the error message as a warning and use the function isLegalAge to print alert another message.


function showAlert(){
	try{
		alerta(islegalAge(age));
	}
	catch(error){
		console.log(typeof error);
		console.warn(error.message);
	}
	finally{
		alert('Intensity updates will show new alert.')
	}
}

showAlert(56);