// Operators
/*
	Add + = Sum
	Subtract - = Difference
	Multiply * = Product
	Divide / = Quotient
	Modulus % = Remainder
*/
function mod(){
	return 9 % 2;
}

console.log(mod());

// 8 * (6 + 2) - (2 * 3)

// Assignment operator (=)
/*
	+= (Addition)
	-= (Subtraction)
	*= (Multiplication)
	/= (Division)
	%= (Modulo)
*/

	let x = 1;
	
	let sum = 1;
	//sum = sum + 4;
	sum += 4;

	console.log(sum);

// Increment and Decrement (++, --)
// Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to

let z = 1;

let increment = ++z;

// Pre-Increment
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z); 

// Post-increment
increment = z++;
console.log("Result of post-increment: " + increment);
console.log("Result of post-increment: " + z);

// Pre-Decrement
let decrement = --z;
console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrement: " + z);

// Post-Decrement
decrement = z--;
console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrement: " + z);

// Comparison Operators
// Boolean Operator (True or False)

// Equality Operator (==)
 
 let juan = 'juan';

console.log(1 == 1);
console.log(0 == false); 
console.log('juan' == juan);

// Strict equality (===)
console.log(1 === true);

// Inequality Operator (!= or ! =)
// Negating certain value is not equal to this certain value

console.log("Inequality Operator");
console.log(1 != 1);
console.log('Juan' != juan);

// Strict Inequality (!== or ! = =)
console.log(0 !== false);

// Other comparison operators
/*
	> - Greater than
	< - Less than
	>= or > = - greater than or equal
	<= or < = - Less than or equal
 */

// Logical Operators
let islegalAge = true;
let isRegistered = false;

/*
	AND Operator (&&) - returns true if all operands are true
		true && true = true
		false && true = false
		true && false = false
		false && false = false

	OR Operator (||) - returns true if at least one operands is true
		true && true = true
		false && true = true
		true && false = true
		false && false = false
 */

// AND Operator
let allRequirementsMet = islegalAge && isRegistered;
console.log("Result of logical AND operator: " + allRequirementsMet);

// OR Operator
let someRequirementsMet = islegalAge || isRegistered;
console.log("Result of logical OR operator: " + someRequirementsMet);

// Selection Control Structures

/*IF Statement
	- execute a statement if a specified condition is true
	Syntax
	if(condition){
		statement/s;
	}
*/

let num = -1;

if(num < 0){
	console.log('Hello');
}

// Create an if statement that will display the words "Welcome to Zuitt if a certain value is greate that or equal to 10."

let num2 = 10;

if(num2 >= 10){
	console.log('Welcome to Zuitt');
}

/*IF-ELSE Statement
	- executes a statement if tha previous condition returns false.
	Syntax:
	if(condition){
		statement/s;
	}
	else{
		statement/s;
	}
 */

num=5;
	if(num >= 10){
		console.log("Number is greater than or equal to 10")
	}
	else{
		console.log("Number is not greater than or equal to 10")
	}

/*
	Mini-Activity
		Create a condition that will return the statement "Senior Age" if age is greater than 59, otherwise display the statement "Invalid age."
 */

/*let age = parseInt(prompt("Please provide age:"));
	if(age > 59){
		alert("Senior Age")
	}
	else{
		alert("Invalid Age")
	}*/

// IF-ELSEIF-ELSE Statement
/*
	Syntax:
	if (condition){
		statement/s;
	}
	else if(condition){
		statement/s;
	}
	else if(condition){
		statement/s;
	}
	.
	.
	.
	else{
		statement/s;
	}
	1 - Quezon City
	2 - Valenzuela City
	3 - Pasig City
	4 - Taguig City
 */

/*let city = parseInt(prompt("Enter a number: "));
if (city === 1){
		alert("Welcome to Quezon City");
	}
	else if(city === 2){
		alert("Welcome to Valenzuela City")
	}
	else if(city === 3){
		alert("Welcome to Pasig City")
	}
	else if(city === 4){
		alert("Welcome to Taguig City")
	}
	else{
		alert("Invalid number");
	}*/

let message = '';

function determineTyphoonIntensity(windSpeed){
	if(windSpeed < 30) {
		return 'Not a typhoon yet';
	}
	else if(windSpeed <= 61){
		return 'Tropical depression detected.';
	}
	else if(windSpeed >= 61 && windSpeed <= 88){
		return 'Tropical storm detected.';
	}
	else if(windSpeed >= 89 && windSpeed <= 117){
		return 'Severe tropical storm detected';
	}
	else{
		return 'Typhoon Dectected.';
	}
}

message = determineTyphoonIntensity(70);
console.log(message);

// Ternary Operator
/*
	Syntax:
	(condition) ? ifTrue : ifFalse
 */

let ternaryResult = (1 < 18) ? 'valid' : 'invalid';
console.log("Result of Ternary Operator: " + ternaryResult);

let name;

function isofLegalAge(){
	name = 'John';
	return 'You are of the legal age limit';
}

function isUnderAge(){
	name = 'Jane';
	return 'You are under the age limit';
}

let age = parseInt(prompt("What is your age? "));
let legalAge = (age >= 18) ? isofLegalAge() : isUnderAge()
alert("Result of Ternary Operator in functions: " + legalAge + ', ' + name);

// Switch Statement
/*
	Syntax:
		switch (expression){
			case value:
				statement/s;
				break;
			case value2:
				statement/s;
				break;
			case valueN:
				statement/s;
				break;
			default:
				statement/s;
		}
 */

let day = prompt("what day of the week is it today?").toLowerCase();

switch (day){
	case 'sunday':
		alert("The color of the day is red");
		break;
	case 'monday':
		alert("The color of the day is orange");
		break;
	case 'tuesday':
		alert("The color of the day is yellow");
		break;
	case 'wednesday':
		alert("The color of the day is green");
		break;
	case 'thursday':
		alert("The color of the day is blue");
		break;
	case 'friday':
		alert("The color of the day is");
		break;
	case 'saturday':
		alert("The color of the day is");
		break;
	default:
		alert("Please input valid day");
}
// Try-Catch-Finally Statement - commonly used for error handling

function showIntensityAlert(windSpeed){
	try{
		alert(determineTyphoonIntensity(windSpeed));
	}
	catch(error){
		console.log(typeof error);
		console.warn(error.message);
	}
	finally{
		alert('Intensity updates will show new alert.')
	}
}

showIntensityAlert(56);
